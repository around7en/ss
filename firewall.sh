firewall-cmd --permanent --zone=public --add-port=1225/tcp
firewall-cmd --permanent --zone=public --add-port=1226/tcp
firewall-cmd --permanent --zone=public --add-port=1227/tcp
firewall-cmd --permanent --zone=public --add-port=1228/tcp
firewall-cmd --permanent --zone=public --add-port=39812/tcp
firewall-cmd --reload
