#! /bin/bash
cp config.json /etc/v2ray/

#curl https://getcaddy.com | bash -s personal
mkdir -p /var/www/xueshen.org
echo '<h1>Hello World!</h1>' | sudo tee /var/www/xueshen.org/index.html
mkdir -p /etc/ssl/caddy
chmod 0770 /etc/ssl/caddy
mkdir -p /etc/caddy

if [ "$1" = "v1" ]; then
	echo "Setup caddy v1 env"
	cp -f caddy1/Caddyfile /etc/caddy
	cp -f caddy1/caddy.service /etc/systemd/system/
fi
if [ "$1" = "v2" ]; then
	echo "Setup caddy v2 env"
	cp -f caddy2/Caddyfile2 /etc/caddy/Caddyfile
	cp -f caddy2/caddy2.service /etc/systemd/system/caddy.service
fi

echo "Starting caddy service"
systemctl daemon-reload
systemctl stop  caddy.service
systemctl start caddy.service
systemctl enable caddy.service
echo "DONE"
